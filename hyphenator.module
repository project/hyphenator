<?php

/**
 * @file
 * Module hook implementations.
 */

/**
 * Define hyphenator library url
 */
define('HYPHENATOR_WEBSITE_URL', 'https://github.com/mnater/hyphenator');

/**
 * Implements hook_init().
 */
function hyphenator_init() {
  static $global_settings_added = FALSE;

  if (!$global_settings_added) {
    $global_settings_added = TRUE;
    drupal_add_library('hyphenator', 'hyphenator');
    drupal_add_library('hyphenator', 'drupal.hyphenator');
  }
}

/**
 * Implements hook_menu().
 */
function hyphenator_menu() {
  $items = array();

  $items['admin/config/content/hyphenator'] = array(
    'title' => 'Hyphenator',
    'description' => 'Change how and where hyphenator behave.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('hyphenator_admin_settings'),
    'access arguments' => array('administer hyphenator'),
    'file' => 'hyphenator.admin.inc',
  );

  $items['admin/config/content/hyphenator/add'] = array(
    'title' => 'Add exception',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('hyphenator_admin_settings_exception_edit_form'),
    'access arguments' => array('administer hyphenator'),
    'file' => 'hyphenator.admin.inc',
  );

  $items['admin/config/content/hyphenator/edit/%'] = array(
    'title' => 'Edit exception',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('hyphenator_admin_settings_exception_edit_form', 5),
    'access arguments' => array('administer hyphenator'),
    'file' => 'hyphenator.admin.inc',
  );

  $items['admin/config/content/hyphenator/delete/%'] = array(
    'title' => 'Delete exception',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('hyphenator_admin_settings_exception_delete_form', 5),
    'access arguments' => array('administer hyphenator'),
    'file' => 'hyphenator.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function hyphenator_permission() {
  return array(
    'administer hyphenator' => array(
      'title' => t('Administer Hyphenator settings'),
    ),
  );
}

/**
 * Implements hook_library().
 */
function hyphenator_library() {
  $library_path = hyphenator_get_library_path();

  $info['hyphenator'] = array(
    'title' => 'Hyphenator',
    'website' => HYPHENATOR_WEBSITE_URL,
    'version' => '1.0.0',
    'js' => array(
      $library_path . '/Hyphenator.js' => array('group' => 'JS_LIBRARY'),
    ),
  );

  // All the settings that are actually passed through into the hyphenator()
  // function are contained in this array.
  $options = array();

  foreach (_hyphenator_get_js_default_config() as $key => $value) {
    $option = variable_get('hyphenator_config_' . $key, $value['default']);

    switch ($value['type']) {
      case 'select':
      case 'number':
        $options[$key] = (int) $option;
        break;

      case 'boolean':
        $options[$key] = ($option == 'true') ? TRUE : FALSE;

        break;

      default:
        if (!empty($option)) {
          $options[$key] = $option;
        }
        break;
    }
  }

  $info['drupal.hyphenator'] = array(
    'title' => 'Drupal Hyphenator integration',
    'website' => 'https://drupal.org/project/hyphenator',
    'version' => '1.0.0',
    'js' => array(
      drupal_get_path('module', 'hyphenator') . '/hyphenator.js' => array(
        'group' => JS_DEFAULT,
        'weight' => 100,
      ),
      array(
        'data' => array(
          'hyphenator' => array(
            'selector' => variable_get('hyphenator_config_selector', '.hyphenate'),
            'options' => $options,
            'exceptions' => variable_get('hyphenator_exceptions', array()),
          ),
        ),
        'type' => 'setting',
      ),
    ),
    'dependencies' => array(
      array('hyphenator', 'hyphenator'),
    ),
  );

  return $info;
}

/**
 * Get the location of the hyphenator library.
 */
function hyphenator_get_library_path() {
  if (function_exists('libraries_get_path')) {
    return libraries_get_path('hyphenator');
  }

  // The following logic is taken from libraries_get_libraries()
  $searchdir = array();

  // Similar to 'modules' and 'themes' directories inside an installation
  // profile, installation profiles may want to place libraries into a
  // 'libraries' directory.
  $searchdir[] = 'profiles/' . drupal_get_profile() . '/libraries';

  // Always search sites/all/libraries.
  $searchdir[] = 'sites/all/libraries';

  // Also search sites/<domain>/*.
  $searchdir[] = conf_path() . '/libraries';

  foreach ($searchdir as $dir) {
    if (file_exists($dir . '/hyphenator/Hyphenator.js')) {
      return $dir . '/hyphenator';
    }
  }

  return FALSE;
}

/**
 * Hold a structure reflecting the settings of Hyphenator.js.
 */
function _hyphenator_get_js_default_config() {
  return array(
    'donthyphenateclassname' => array(
      'title' => 'Exclude class',
      'type' => 'string',
      'default' => 'donthyphenate',
      'description' => t("A string containing the css-class-name for elements that should not be hyphenated. Default: 'donthyphenate'"),
    ),
    'urlclassname' => array(
      'title' => 'URL class name',
      'type' => 'string',
      'default' => 'urlhyphenate',
      'description' => t("URLs are automatically detected and Hyphenator.js inserts zero width spaces at good breaking points. Default: 'urlhyphenate'"),
    ),
    'minwordlength' => array(
      'title' => 'Min word length',
      'type' => 'number',
      'default' => 6,
      'description' => t('A number wich indicates the minimal length of words to hyphenate. Default: 6'),
    ),
    'hyphenchar' => array(
      'title' => 'Hyphenchar',
      'type' => 'string',
      'default' => '',
      'description' => t('To check hyphenation you can set hyphenchar to a visible character.'),
    ),
    'urlhyphenchar' => array(
      'title' => 'URL hyphenchar',
      'type' => 'string',
      'default' => '',
      'description' => t('When processing URLs and email-adresses a hyphen would invalidate the text.'),
    ),
    'displaytogglebox' => array(
      'title' => 'Display togglebox',
      'type' => 'boolean',
      'default' => 'false',
      'description' => t("If you want the user to be able to turn off an back on hyphenation, you can display the 'toggleBox'."),
    ),
    'remoteloading' => array(
      'title' => 'Remote loading',
      'type' => 'boolean',
      'default' => 'true',
      'description' => t('Hyphenator.js automatically loads the pattern files for each language, if they are available. Alternatively you can load the pattern files manually and disable the remote loading mechanism.'),
    ),
    'enablecache' => array(
      'title' => 'Enable cache',
      'type' => 'boolean',
      'default' => 'true',
      'description' => t('Hyphenator caches words that have been hyphenated to fasten execution.'),
    ),
    'enablereducedpatternset' => array(
      'title' => 'Enable reduced pattern set',
      'type' => 'boolean',
      'default' => 'false',
      'description' => t('This property is used by the reducePatternSet-tool. It stores all patterns used in a run in the language-object. They can be retrieved with the Hyphenator.getRedPatternSet(lang)-method.'),
    ),
    'intermediatestate' => array(
      'title' => 'Intermediate state',
      'type' => 'string',
      'default' => 'hidden',
      'description' => t("When a paragraph is hyphenated, the browser does a reflow of the layout, which you might like or not. Default: 'hidden'"),
    ),
    'safecopy' => array(
      'title' => 'Safe copy',
      'type' => 'boolean',
      'default' => 'true',
      'description' => t('This property this property tells Hyphenator.js if it should remove hyphenation from copied text.'),
    ),
    'doframes' => array(
      'title' => 'Do frames',
      'type' => 'boolean',
      'default' => 'false',
      'description' => t('Hyphenator does not automatically hyphenate text in frames nor iframes.'),
    ),
    'storagetype' => array(
      'title' => 'Storage type',
      'type' => 'string',
      'default' => 'local',
      'description' => t("To speed up Hyphenator.js the pattern files are stored in DOM storage, if the browser supports it. You can change this to sessionStorage (option 'session') or turn this feature off (by setting storagetype to 'none'. Default: 'local'"),
    ),
    'orphancontrol' => array(
      'title' => 'Orphan control',
      'type' => 'select',
      'list' => array(
        1 => '1 (default): last word is hyphenated',
        2 => '2: last word is not hyphenated',
        3 => '3: last word is not hyphenated and last space is non breaking',
      ),
      'default' => 1,
      'description' => t('In some cases it may happen that one single syllable comes to the last line'),
    ),
    'dohyphenation' => array(
      'title' => 'Do hyphenation',
      'type' => 'boolean',
      'default' => 'true',
      'description' => t('Sometimes you like to run the script, prepare everything, but not to hyphenate yet. In this case you can set the option dohyphenation to false.'),
    ),
    'persistentconfig' => array(
      'title' => 'Persistent config',
      'type' => 'boolean',
      'default' => 'false',
      'description' => t('With the option persistentconfig set to true, all configuration options are stored in a object called Hyphenator_config in the storage type defined by the property storagetype.'),
    ),
    'defaultlanguage' => array(
      'title' => 'Default language',
      'type' => 'string',
      'default' => '',
      'description' => t("If you can't set a lang-attribute to the appropriate html-tag it may be interesting to set the default language."),
    ),
    'useCSS3hyphenation' => array(
      'title' => 'Use CSS3 hyphenation',
      'type' => 'boolean',
      'default' => 'false',
      'description' => t('In cases where CSS3-Hyphenation is supported by the browser, Hyphenator.js can activate it for the supported languages instead of computing the hyphenation points by itself.'),
    ),
    'unhide' => array(
      'title' => 'Unhide',
      'type' => 'string',
      'default' => 'wait',
      'description' => t("By default – if intermediatestate is set to the default hidden – Hyphenator.js waits until all elements are processed and finally unhides them. In some cases a progressive unhiding is more usefull. Default: 'wait'"),
    ),
  );
}

/**
 * Build an array of the language strings found in the Hyphenator.js library.
 */
function _hyphenator_get_available_languages() {
  static $languages;

  if (isset($languages)) {
    return $languages;
  }

  $library_path = hyphenator_get_library_path();
  $patterns = $library_path . '/patterns';
  $languages = array();

  if (is_dir($patterns)) {
    if ($handle = opendir($patterns)) {
      while (FALSE !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
          $language = basename($entry, '.js');
          $languages[$language] = $language;
        }
      }
      closedir($handle);
    }
  }

  asort($languages);
  return $languages;
}
