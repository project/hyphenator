<?php

/**
 * @file
 * Drush integration for hyphenator.
 */

/**
 * The hyphenator plugin URI.
 */
define('HYPHENATOR_DOWNLOAD_URI', 'https://github.com/mnater/Hyphenator/archive/5.1.0.zip');

/**
 * Implements hook_drush_command().
 */
function hyphenator_drush_command() {
  $items = array();

  // The key in the $items array is the name of the command.
  $items['hyphenator-plugin'] = array(
    'callback' => 'drush_hyphenator_plugin',
    'description' => dt('Download and install the hyphenator plugin.'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'arguments' => array(
      'path' => dt('Optional. A path where to install the hyphenator plugin. If omitted Drush will use the default location.'),
    ),
    'aliases' => array('hyphenatorplugin'),
  );

  return $items;
}

/**
 * Implements hook_drush_help().
 */
function hyphenator_drush_help($section) {
  switch ($section) {
    case 'drush:hyphenator-plugin':
      return dt('Download and install the hyphenator plugin from http://harvesthq.github.com/hyphenator, default location is sites/all/libraries.');
  }

  return NULL;
}

/**
 * Command to download the hyphenator plugin.
 */
function drush_hyphenator_plugin() {
  $args = func_get_args();
  if (!empty($args[0])) {
    $path = $args[0];
  }
  else {
    $path = 'sites/all/libraries';
  }

  // Create the path if it does not exist.
  if (!is_dir($path)) {
    drush_op('mkdir', $path);
    drush_log(dt('Directory @path was created', array('@path' => $path)), 'notice');
  }

  // Set the directory to the download location.
  $olddir = getcwd();
  chdir($path);

  // Download the zip archive.
  if ($filepath = drush_download_file(HYPHENATOR_DOWNLOAD_URI)) {
    $filename = basename($filepath);
    $dirname = basename($filepath, '.zip');

    // Remove any existing hyphenator plugin directory.
    if (is_dir($dirname) || is_dir('hyphenator')) {
      drush_delete_dir($dirname, TRUE);
      drush_delete_dir('hyphenator', TRUE);
      drush_log(dt('A existing hyphenator plugin was deleted from @path', array('@path' => $path)), 'notice');
    }

    // Decompress the zip archive.
    drush_tarball_extract($filename, $dirname);

    if ($handle = opendir($dirname)) {
      while (FALSE !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
          if (file_exists($dirname . '/' . $entry . '/Hyphenator.js')) {
            drush_move_dir($dirname . '/' . $entry, 'hyphenator', TRUE);
            break;
          }
        }
      }
      closedir($handle);
    }

    if ($dirname != 'hyphenator') {
      drush_delete_dir($dirname, TRUE);
      $dirname = 'hyphenator';
    }
  }

  if (is_dir($dirname)) {
    drush_log(dt('hyphenator plugin has been installed in @path', array('@path' => $path)), 'success');
  }
  else {
    drush_log(dt('Drush was unable to install the hyphenator plugin to @path', array('@path' => $path)), 'error');
  }

  // Set working directory back to the previous working directory.
  chdir($olddir);
}
