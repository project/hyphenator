(function ($) {

  Drupal.behaviors.hyphenator = {
    attach: function (context, settings) {
      // Checking settings.
      if (!settings.hyphenator || settings.hyphenator.selector === '') {
        return;
      }

      var options = settings.hyphenator.options;
      var exceptions = settings.hyphenator.exceptions;

      // Replace selector function.
      options.selectorfunction = function () {
        return $(settings.hyphenator.selector, context).once('hyphenator').get();
      };

      // Exceptions.
      $.each(exceptions, function (lang, words) {
        if (lang == 'GLOBAL') {
          lang = '';
        }
        Hyphenator.addExceptions(lang, words);
      });

      Hyphenator.config(options);
      Hyphenator.run();
    }
  };

})(jQuery);
