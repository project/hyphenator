<?php

/**
 * @file
 * Module administration pages.
 */

/**
 * Admin hyphenator module form.
 */
function hyphenator_admin_settings($form, &$form_state) {
  if (!hyphenator_get_library_path()) {
    drupal_set_message(t('The library could not be detected. You need to download the !hyphenator and extract the entire contents of the archive into the %path directory on your server.',
      array(
        '!hyphenator' => l(t('Hyphenator JavaScript file'), HYPHENATOR_WEBSITE_URL),
        '%path' => 'sites/all/libraries',
      )), 'error');

    return FALSE;
  }

  $form = array();

  $form['configure'] = array(
    '#type' => 'fieldset',
    '#title' => t('Configure how <em>Hyphenator.js</em> must operate'),
    '#description' => t('Those parameters permit to customize how <em>Hyphenator.js</em> must operate (see !hyphenator_site for full documentation).', array('!hyphenator_site' => l('Hyphenator.js web site', HYPHENATOR_WEBSITE_URL))),
  );

  $form['configure']['hyphenator_config_selector'] = array(
    '#type' => 'textarea',
    '#title' => t('Apply Hyphenat to the following elements'),
    '#description' => t("A comma-separated list of jQuery selectors to apply Hyphenation. Default: '.hyphenate'"),
    '#default_value' => variable_get('hyphenator_config_selector', '.hyphenate'),
  );

  $form['configure']['hyphenator_config'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => t('Config parameters'),
  );

  foreach (_hyphenator_get_js_default_config() as $key => $value) {
    switch ($value['type']) {
      case 'string':
      case 'number':
        $form['configure']['hyphenator_config']['hyphenator_config_' . $key] = array(
          '#type' => 'textfield',
        );
        break;

      case 'select':
        $form['configure']['hyphenator_config']['hyphenator_config_' . $key] = array(
          '#type' => 'select',
          '#options' => $value['list'],
        );
        break;

      case 'boolean':
        $form['configure']['hyphenator_config']['hyphenator_config_' . $key] = array(
          '#type' => 'select',
          '#options' => array(
            'true' => t('Enabled!d', array('!d' => ($value['default'] == 'true') ? ' (default)' : '')),
            'false' => t('Disabled!d', array('!d' => ($value['default'] == 'false') ? ' (default)' : '')),
          ),
        );
        break;
    }

    if (array_key_exists('hyphenator_config_' . $key, $form['configure']['hyphenator_config'])) {
      $form['configure']['hyphenator_config']['hyphenator_config_' . $key]['#title'] = $value['title'];
      $form['configure']['hyphenator_config']['hyphenator_config_' . $key]['#default_value'] = variable_get('hyphenator_config_' . $key, $value['default']);
      $form['configure']['hyphenator_config']['hyphenator_config_' . $key]['#description'] = $value['description'];
    }
  }

  $form['configure']['hyphenator_exceptions'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#title' => t('Exceptions'),
    '#description' => t('Add words here that you find are wrongly hyphenated by <em>Hyphenator.js</em>.'),
  );

  $rows = array();
  $exceptions = variable_get('hyphenator_exceptions', array());

  foreach ($exceptions as $key => $value) {
    $rows[] = array(
      $key,
      $value,
      l(t('edit'), 'admin/config/content/hyphenator/edit/' . $key),
      l(t('delete'), 'admin/config/content/hyphenator/delete/' . $key),
    );
  }
  $form['configure']['hyphenator_exceptions']['add'] = array(
    '#type' => 'item',
    '#theme' => 'links',
    '#links' => array(
      'add' => array(
        'title' => t('Add an exception'),
        'href' => 'admin/config/content/hyphenator/add',
      ),
    ),
    '#attributes' => array('class' => 'action-links'),
  );
  $form['configure']['hyphenator_exceptions']['table'] = array(
    '#type' => 'markup',
    '#theme' => 'table',
    '#header' => array(
      t('Language'),
      t('Words'),
      array('data' => t('Operations'), 'colspan' => 2),
    ),
    '#empty' => t('No exceptions recorded.'),
    '#rows' => $rows,
  );

  return system_settings_form($form);
}

/**
 * Exceptions edit form.
 */
function hyphenator_admin_settings_exception_edit_form($form, &$form_state, $language = NULL) {
  if (is_null($language)) {
    $language = 'GLOBAL';
    $words = '';
    $new = TRUE;
  }
  else {
    $exceptions = variable_get('hyphenator_exceptions', array());
    $words = (array_key_exists($language, $exceptions)) ? $exceptions[$language] : '';
    $new = FALSE;
  }

  $languages = _hyphenator_get_available_languages();
  $form = array();

  if (!empty($languages)) {
    $languages = array_merge(array('GLOBAL' => 'GLOBAL'), $languages);
  }

  $form['message'] = array(
    '#markup' => ($new) ? t('Add one or more exception words for a specified language.') : t('Modify the exception words list for the specified language.'),
  );

  $form['language'] = array(
    '#title' => t('Language'),
    '#disabled' => !$new,
    '#description' => t("Use 'GLOBAL' !blato enable the associated words to be skipped for any languages.", array('!bla' => empty($languages) ? t('or an empty string') . ' ' : '')),
    '#default_value' => $language,
  );

  if (!empty($languages)) {
    $languages = array_merge(array('GLOBAL' => 'GLOBAL'), $languages);
    $form['language']['#type'] = 'select';
    $form['language']['#options'] = $languages;
  }
  else {
    $form['language']['#type'] = 'textfield';
    $form['language']['#size'] = 10;
    $form['language']['#maxlength'] = 15;
  }

  $form['exception_words'] = array(
    '#type' => 'textarea',
    '#title' => t('Exception words'),
    '#description' => t('Begin a newline to add a word.'),
    '#default_value' => $words,
  );

  $form['exception_new'] = array(
    '#type' => 'hidden',
    '#value' => $new,
  );

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save exception'),
  );

  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => 'admin/config/content/hyphenator',
  );

  return $form;
}

/**
 * Exceptions edit form validation.
 */
function hyphenator_admin_settings_exception_edit_form_validate($form, $form_state) {
  $language = $form_state['values']['language'];
  $new = $form_state['values']['exception_new'];
  $exceptions = variable_get('hyphenator_exceptions', array());

  if (empty($language)) {
    $language = 'GLOBAL';
  }

  if (array_key_exists($language, $exceptions) && $new) {
    form_set_error('language', t('The Language %lang that you submitted already exists.', array('%lang' => $language)));
  }

  if (!preg_match('/^(\s*\S+\s*)+$/', $form_state['values']['exception_words'])) {
    form_set_error('exception_words', t("The 'Exception words' field must be set with some strings."));
  }
}

/**
 * Exceptions edit form submission.
 */
function hyphenator_admin_settings_exception_edit_form_submit($form, &$form_state) {
  $form_state['redirect'] = 'admin/config/content/hyphenator';
  $language = $form_state['values']['language'];
  $words = $form_state['values']['exception_words'];

  if (empty($language)) {
    $language = 'GLOBAL';
  }

  $exceptions = variable_get('hyphenator_exceptions', array());
  $exceptions[$language] = trim($words);
  variable_set('hyphenator_exceptions', $exceptions);
  drupal_set_message(t("The hyphenator exception language '%lang' have been saved.", array('%lang' => $language)));
}

/**
 * Exceptions delete form validation.
 */
function hyphenator_admin_settings_exception_delete_form($form, &$form_state, $language) {
  $exceptions = variable_get('hyphenator_exceptions', array());
  $words = array_key_exists($language, $exceptions) ? $exceptions[$language] : '';
  $question = t('Are you sure you want to delete de Hyphenator language %l and its exception(s)?', array('%l' => $language));
  $path = 'admin/config/content/hyphenator';
  $description = t('<p>The language %lang hold the following exception word(s): %words.</p><p>This action cannot be undone.</p>', array(
    '%lang' => $language,
    '%words' => $words,
  ));

  $form = array();
  $form['language'] = array('#type' => 'hidden', '#value' => $language);

  return confirm_form($form, $question, $path, $description, t('Delete'), t('Cancel'));
}

/**
 * Exceptions delete form submission.
 */
function hyphenator_admin_settings_exception_delete_form_submit($form, &$form_state) {
  $form_state['redirect'] = 'admin/config/content/hyphenator';
  $exceptions = variable_get('hyphenator_exceptions', array());
  $language = $form_state['values']['language'];

  if (empty($language)) {
    $language = 'GLOBAL';
  }

  unset($exceptions[$language]);
  variable_set('hyphenator_exceptions', $exceptions);
  drupal_set_message(t("The hyphenator language %lang and its exception(s) have been deleted.", array('%lang' => $form_state['values']['language'])));
}
